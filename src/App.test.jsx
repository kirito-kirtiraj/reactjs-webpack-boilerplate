import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import 'cross-fetch/polyfill'
import App from './App'

describe('App.jsx', () => {
  test('should render correctly', () => {
    const { getByTestId } = render(<App />)
    expect(getByTestId(/characters/i)).toBeInTheDocument()
  })
})
