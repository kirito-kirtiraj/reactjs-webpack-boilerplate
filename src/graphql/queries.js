import { gql } from '@apollo/client'

export const GET_CHARACTERS = gql`
  query {
    characters {
      info {
        next
      }
      results {
        id
        name
        status
        species
        gender
        origin {
          name
        }
        location {
          name
        }
        image
        created
      }
    }
  }
`
