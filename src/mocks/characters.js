import { GET_CHARACTERS } from '../graphql/queries'

export const character = {
  id: '1',
  name: 'Rick Sanchez',
  status: 'Alive',
  species: 'Human',
  gender: 'Male',
  'origin.name': 'Earth (C-137)',
  'location.name': 'Earth (Replacement Dimension)',
  image: 'https://rickandmortyapi.com/api/character/avatar/1.jpeg',
  created: '2017-11-04T18:48:46.250Z'
}

export const mockGraphqlRequest = [
  {
    request: {
      query: GET_CHARACTERS
    },
    result: {
      data: {
        characters: {
          info: {
            next: 2
          },
          results: [
            {
              id: '1',
              name: 'Rick Sanchez',
              status: 'Alive',
              species: 'Human',
              gender: 'Male',
              origin: {
                name: 'Earth (C-137)'
              },
              location: {
                name: 'Earth (Replacement Dimension)'
              },
              image: 'https://rickandmortyapi.com/api/character/avatar/1.jpeg',
              created: '2017-11-04T18:48:46.250Z'
            }
          ]
        }
      }
    }
  }
]
