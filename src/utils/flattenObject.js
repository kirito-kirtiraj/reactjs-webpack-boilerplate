const flattenObject = (objectToFlatten) => {
  const result = {}

  for (let key in objectToFlatten) {
    if (typeof objectToFlatten[key] === 'object') {
      const temp = flattenObject(objectToFlatten[key])
      for (const innerKey in temp) {
        result[key + '.' + innerKey] = temp[innerKey]
      }
    } else {
      result[key] = objectToFlatten[key]
    }
  }
  return result
}

export default flattenObject
