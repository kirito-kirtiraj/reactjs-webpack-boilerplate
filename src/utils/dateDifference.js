import diffDates from 'diff-dates'

const calculateDateDifference = (date) =>
  diffDates(new Date(), new Date(date), 'years')

export default calculateDateDifference
