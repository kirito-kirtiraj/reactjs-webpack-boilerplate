import React from 'react'
import { Characters } from './components/organisms'
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client'
import './styles/main.scss'

const client = new ApolloClient({
  uri: 'https://rickandmortyapi.com/graphql',
  cache: new InMemoryCache()
})

const App = () => {
  return (
    <ApolloProvider client={client}>
      <Characters />
    </ApolloProvider>
  )
}

export default App
