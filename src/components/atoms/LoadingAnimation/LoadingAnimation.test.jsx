import { render } from '@testing-library/react'
import '@testing-library/jest-dom'

import { LoadingAnimation } from './index'

describe('<LoadingAnimation />', () => {
  test('should render correctly', () => {
    const { getByTitle } = render(<LoadingAnimation />)
    expect(getByTitle(/Loading icon/i)).toBeInTheDocument()
  })
})
