import { LoadingAnimation } from './index'

export default {
  title: 'Atoms/LoadingAnimation',
  component: LoadingAnimation
}

const Template = () => <LoadingAnimation />

export const Default = Template.bind({})
