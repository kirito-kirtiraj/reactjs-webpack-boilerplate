import React from 'react'

import LoadingIcon from '../../../assets/images/loading.svg'
import './styles.scss'

export const LoadingAnimation = () => (
  <LoadingIcon title='Loading icon' className='loading-icon' />
)
