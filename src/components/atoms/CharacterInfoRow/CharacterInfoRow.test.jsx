import { render } from '@testing-library/react'
import '@testing-library/jest-dom'

import { CharacterInfoRow } from './index'

describe('<CharacterInfoRow />', () => {
  test('should render correctly', () => {
    const { getByText } = render(
      <CharacterInfoRow title='title' value='some value' />
    )
    expect(getByText(/title/i)).toBeInTheDocument()
  })
})
