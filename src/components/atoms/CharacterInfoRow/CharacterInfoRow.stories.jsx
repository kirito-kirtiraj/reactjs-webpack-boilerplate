import { CharacterInfoRow } from './index'

export default {
  title: 'Atoms/CharacterInfoRow',
  component: CharacterInfoRow,
  argTypes: {
    title: '',
    value: ''
  }
}

const Template = (args) => <CharacterInfoRow {...args} />

export const Default = Template.bind({})
Default.args = {
  title: 'Status',
  value: 'Some status'
}
