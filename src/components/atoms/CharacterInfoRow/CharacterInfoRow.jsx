import React from 'react'
import PropTypes from 'prop-types'
import './styles.scss'

export const CharacterInfoRow = ({ title, value }) => (
  <div className='character-info'>
    <p className='character-info__title'>{title}</p>
    <p className='character-info__value'>{value}</p>
  </div>
)

CharacterInfoRow.propTypes = {
  title: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired
}
