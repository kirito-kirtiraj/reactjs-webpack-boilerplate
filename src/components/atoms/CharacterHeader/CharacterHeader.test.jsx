import { render } from '@testing-library/react'
import '@testing-library/jest-dom'

import { CharacterHeader } from './index'

describe('<CharacterHeader />', () => {
  test('should render correctly', () => {
    const { getByText } = render(
      <CharacterHeader
        id='1'
        imageUrl='https://rickandmortyapi.com/api/character/avatar/1.jpeg'
        name='Rick Sanchez'
        created={4}
      />
    )
    expect(getByText(/id: 1/i)).toBeInTheDocument()
    expect(getByText(/created 4 years ago/i)).toBeInTheDocument()
  })
})
