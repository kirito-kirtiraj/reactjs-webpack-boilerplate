import { CharacterHeader } from './index'

export default {
  title: 'Atoms/CharacterHeader',
  component: CharacterHeader,
  argTypes: {
    id: '',
    imageUrl: '',
    name: '',
    created: ''
  }
}

const Template = (args) => <CharacterHeader {...args} />

export const Default = Template.bind({})
Default.args = {
  id: '1',
  imageUrl: 'https://rickandmortyapi.com/api/character/avatar/1.jpeg',
  name: 'Rick Sanchez',
  created: '2017-11-04T18:48:46.250Z'
}
