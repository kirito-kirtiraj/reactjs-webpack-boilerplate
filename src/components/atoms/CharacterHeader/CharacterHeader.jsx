import React from 'react'
import PropTypes from 'prop-types'
import './styles.scss'

export const CharacterHeader = ({ id, imageUrl, name, created }) => (
  <div className='character-header'>
    <img src={imageUrl} alt={name} className='character-header__image' />
    <div className='character-header__info'>
      <h2 className='character-header__name'>{name}</h2>
      <p className='character-header__id-created'>
        id: {id} - created {created} years ago
      </p>
    </div>
  </div>
)

CharacterHeader.propTypes = {
  id: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  created: PropTypes.number.isRequired
}
