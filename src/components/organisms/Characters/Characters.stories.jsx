import { Characters } from './index'

export default {
  title: 'Characters',
  component: Characters
}

const Template = () => <Characters />

export const Default = Template.bind({})
