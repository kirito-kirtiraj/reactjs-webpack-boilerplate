import React, { useEffect, useState } from 'react'
import './styles.scss'
import { useQuery } from '@apollo/client'

import { LoadingAnimation } from '../../atoms'
import { CharacterCard } from '../../molecules'
import flattenObject from '../../../utils/flattenObject'
import { GET_CHARACTERS } from '../../../graphql/queries'

export const Characters = () => {
  const { loading, error, data } = useQuery(GET_CHARACTERS)
  const [characters, setCharacters] = useState([])

  useEffect(() => {
    if (!loading && !error) setCharacters(data.characters.results)
  }, [data, loading, error])

  return (
    <section
      className={
        'characters' + (loading || error ? ' characters--loading' : '')
      }
      data-testid='characters'
    >
      {loading && <LoadingAnimation />}
      {!loading && error && (
        <h1 className='characters__loading-error'>
          There was an error loading the characters
        </h1>
      )}
      {!loading &&
        !error &&
        characters.map((character) => (
          <CharacterCard
            data-testid='character-card'
            key={character.id}
            character={flattenObject(character)}
          />
        ))}
    </section>
  )
}
