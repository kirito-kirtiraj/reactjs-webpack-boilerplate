import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import { MockedProvider } from '@apollo/client/testing'

import { Characters } from './index'
import { mockGraphqlRequest } from '../../../mocks/characters'

describe('<Characters />', () => {
  test('should render correctly', () => {
    const { getByTestId } = render(
      <MockedProvider mocks={mockGraphqlRequest} addTypename={false}>
        <Characters />
      </MockedProvider>
    )
    expect(getByTestId(/characters/i)).toBeInTheDocument()
  })

  test('should render loading animation correctly', () => {
    const { getByTitle } = render(
      <MockedProvider mocks={mockGraphqlRequest} addTypename={false}>
        <Characters />
      </MockedProvider>
    )
    expect(getByTitle(/Loading icon/i)).toBeInTheDocument()
  })

  test('should render character card correctly', async () => {
    const { findByText } = render(
      <MockedProvider mocks={mockGraphqlRequest} addTypename={false}>
        <Characters />
      </MockedProvider>
    )
    const id = await findByText(/id/i)
    expect(id).toBeInTheDocument()
  })
})
