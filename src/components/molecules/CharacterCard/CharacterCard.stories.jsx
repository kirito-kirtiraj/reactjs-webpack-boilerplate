import { CharacterCard } from './index'

export default {
  title: 'Molecules/CharacterCard',
  component: CharacterCard,
  argTypes: {
    character: {}
  }
}

const Template = (args) => <CharacterCard {...args} />

export const Primary = Template.bind({})
Primary.args = {
  character: {
    id: '1',
    name: 'Rick Sanchez',
    status: 'Alive',
    species: 'Human',
    gender: 'Male',
    'origin.name': 'Earth (C-137)',
    'location.name': 'Earth (Replacement Dimension)',
    image: 'https://rickandmortyapi.com/api/character/avatar/1.jpeg',
    created: '2017-11-04T18:48:46.250Z'
  }
}
