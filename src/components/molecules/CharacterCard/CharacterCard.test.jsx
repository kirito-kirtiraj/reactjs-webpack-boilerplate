import { render } from '@testing-library/react'
import '@testing-library/jest-dom'

import { CharacterCard } from './index'
import { character } from '../../../mocks/characters'

describe('<CharacterCard />', () => {
  test('should render correctly', () => {
    const { getByText } = render(<CharacterCard character={character} />)
    expect(getByText(/id: 1/i)).toBeInTheDocument()
  })
})
