import React from 'react'
import PropTypes from 'prop-types'

import { CharacterInfoRow, CharacterHeader } from '../../atoms'
import calculateDateDifference from '../../../utils/dateDifference'
import './styles.scss'

const infoKeys = [
  { title: 'status', key: 'status' },
  { title: 'species', key: 'species' },
  { title: 'gender', key: 'gender' },
  { title: 'origin', key: 'origin.name' },
  { title: 'last location', key: 'location.name' }
]

export const CharacterCard = ({ character }) => (
  <div className='character-card'>
    <CharacterHeader
      id={character.id}
      name={character.name}
      imageUrl={character.image}
      created={calculateDateDifference(character.created)}
    />
    <div className='character-card__info'>
      {infoKeys.map((item) => (
        <CharacterInfoRow
          key={item.key}
          title={item.title}
          value={character[item.key]}
        />
      ))}
    </div>
  </div>
)

CharacterCard.propTypes = {
  character: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    status: PropTypes.string,
    species: PropTypes.string,
    gender: PropTypes.string,
    'origin.name': PropTypes.string,
    'location.name': PropTypes.string,
    image: PropTypes.string,
    created: PropTypes.string
  }).isRequired
}
